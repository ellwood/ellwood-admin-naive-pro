import { fileURLToPath } from 'url'
import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import { VitePluginSingleHmr } from 'vite-plugin-single-hmr'
import vueJsx from '@vitejs/plugin-vue-jsx'
import { visualizer } from 'rollup-plugin-visualizer'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { NaiveUiResolver } from 'unplugin-vue-components/resolvers'
import Unocss from 'unocss/vite'

const baseSrc = fileURLToPath(new URL('./src', import.meta.url))

export default defineConfig(({ mode }) => {
  const env = loadEnv(mode, process.cwd())
  console.log('env==>', env)

  return {
    plugins: [
      vue(),
      VitePluginSingleHmr(),
      vueJsx(),
      visualizer(),
      AutoImport({
        imports: [
          'vue',
          'vue/macros',
          'vue-router',
          '@vueuse/core',
          'pinia',
          'vue-i18n',
          {
            'naive-ui': [
              'useDialog',
              'useMessage',
              'useNotification',
              'useLoadingBar'
            ]
          }
        ],
        // 生成到的地址
        dts: 'types/auto-imports.d.ts',
        // 配置本地需要自动导入的库
        dirs: [
          // pinia状态管理目录
          'src/stores',
          // 自定义组合式api目录
          'src/composables'
        ]
      }),
      Components({
        // 导入naiveui的配置项目
        resolvers: [NaiveUiResolver()],
        // 生成类型的地址
        dts: 'types/components.d.ts'
      }),
      Unocss()
    ],
    resolve: {
      alias: {
        '~': baseSrc,
        '~@': baseSrc,
        '@': baseSrc
      }
    },
    server: {
      // 设置服务启动时自动在浏览器中打开应用程序
      open: false,
      // 设置服务启动时提供的本地IP地址
      host: '0.0.0.0',
      // 设置服务启动的端口号
      port: 3000
    }
  }
})
