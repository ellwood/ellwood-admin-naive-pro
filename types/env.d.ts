/// <reference types="vite/client" />
declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}

interface ImportMetaEnv {
  readonly VITE_APP_TEST_BASE_URL: string
  readonly VITE_APP_PROD_BASE_URL: string
  readonly VITE_APP_DEV_BASE_URL: string
  readonly VITE_APP_BASE: string
  readonly VITE_APP_BASE_URL: string
  readonly VITE_APP_NAME: string
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}
