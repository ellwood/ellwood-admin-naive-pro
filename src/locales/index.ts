/**
 * @ClassName: index
 * @Description: // i18n多语言配置
 * @Author: Ellwood
 * @CreateDate: 2024/4/19 - 16:24
 * @Version: V1.0.0
 */
import { createI18n } from 'vue-i18n'
import zhCN from './lang/zh-CN'

export const defaultLocale = 'zh-CN'

const i18n = createI18n({
  // 是否启用传统模式，默认是true，这里不需要设置为false
  legacy: false,
  // 本地化语言获取失败的时候是否输出警告
  missingWarn: false,
  // 默认语言
  locale: defaultLocale,
  messages: {
    'zh-CN': zhCN
  }
})

export const loadLanguageAsync = async (locale: string = defaultLocale) => {
  const current = i18n.global.locale.value
  try {
    if (current === locale) return nextTick()

    const messages = await import(`./lang/${locale}.ts`)
    if (messages) i18n.global.setLocaleMessage(locale, messages.default)
  } catch (e) {
    console.warn('load language error', e)
  }
  return nextTick()
}

export default i18n
