import { dateZhCN, zhCN } from 'naive-ui'

/**
 * @ClassName: zh-CN
 * @Description: // 中文配置
 * @Author: Ellwood
 * @CreateDate: 2024/4/19 - 16:27
 * @Version: V1.0.0
 */

export default {
  naiveUI: {
    locale: zhCN,
    dateLocale: dateZhCN
  }
}
