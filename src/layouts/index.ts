import Layout from './base-layout/index.vue'

/**
 * @ClassName: index
 * @Description: // 布局根文件
 * @Author: Ellwood
 * @CreateDate: 2024/4/22 - 10:18
 * @Version: V1.0.0
 */
export { Layout }
