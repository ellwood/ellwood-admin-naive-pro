import Logo from './logo.vue'
import Title from './title.vue'
import LayoutContent from './layout-content.vue'
import LayoutSider from './layout-sider.vue'

/**
 * @ClassName: index
 * @Description: // 统一导出公共通用布局组件
 * @Author: Ellwood
 * @CreateDate: 2024/4/22 - 10:51
 * @Version: V1.0.0
 */
export { Logo, Title, LayoutContent, LayoutSider }
