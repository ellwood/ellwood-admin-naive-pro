import { createRouter, createWebHistory } from 'vue-router'
import type { App } from 'vue'
import StaticRoutes from '~/router/staticRoutes.ts'

/**
 * @ClassName: index
 * @Description: //  路由主配置文件
 * @Author: Ellwood
 * @CreateDate: 2024/4/19 - 16:04
 * @Version: V1.0.0
 */
const router = createRouter({
  history: createWebHistory(),
  routes: [...StaticRoutes]
})

export function setupRouter(app: App) {
  app.use(router)
}

export default router
