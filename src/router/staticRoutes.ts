/**
 * @ClassName: staticRoutes
 * @Description: // 静态路由配置
 * @Author: Ellwood
 * @CreateDate: 2024/4/19 - 16:07
 * @Version: V1.0.0
 */
import type { RouteRecordRaw } from 'vue-router'
import { Layout } from '~/layouts'

const StaticRoutes = [
  {
    path: '/',
    name: 'index',
    component: Layout,
    redirect: '/index',
    children: [
      {
        path: '/index',
        name: 'Index',
        component: () => import('~/pages/index.vue'),
        meta: { title: 'Index' }
      }
    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('~/pages/login.vue')
  }
] as RouteRecordRaw[]

export default StaticRoutes
