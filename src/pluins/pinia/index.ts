/**
 * @ClassName: index
 * @Description: // pinia配置
 * @Author: Ellwood
 * @CreateDate: 2024/4/19 - 16:20
 * @Version: V1.0.0
 */
import type { App } from 'vue'

export function setupPinia(app: App) {
  const pinia = createPinia()

  app.use(pinia)
}
