import type { App } from 'vue'
import '~/assets/style/index.css'
import { setupPinia } from '~/pluins/pinia'
import { setupI18n } from '~/pluins/i18n'
import { setupUnocss } from '~/pluins/UnoCss'

/**
 * @ClassName: index
 * @Description: // 全局插件配置
 * @Author: Ellwood
 * @CreateDate: 2024/4/19 - 15:06
 * @Version: V1.0.0
 */
export function setupPlugins(app: App) {
  setupUnocss()
  setupPinia(app)
  setupI18n(app)
}
