/**
 * @ClassName: index
 * @Description: // 多语言插件配置
 * @Author: Ellwood
 * @CreateDate: 2024/4/19 - 16:37
 * @Version: V1.0.0
 */
import type { App } from 'vue'
import i18n from '~/locales'

export function setupI18n(app: App) {
  app.use(i18n)
}
