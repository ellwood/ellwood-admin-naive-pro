/**
 * @ClassName: index
 * @Description: // unocss配置
 * @Author: Ellwood
 * @CreateDate: 2024/4/28 - 11:53
 * @Version: V1.0.0
 */
import '@unocss/reset/tailwind.css'
import 'uno.css'

export function setupUnocss() {}
