import logo from '~/assets/vue.svg'

/**
 * @ClassName: layout-theme
 * @Description: // 布局全局配置项
 * @Author: Ellwood
 * @CreateDate: 2024/4/22 - 9:56
 * @Version: V1.0.0
 */

export interface LayoutType {
  id: string
  key: 'mix' | 'side' | 'top'
  title: string
  inverted?: boolean
  dark?: boolean
}

export interface LayoutTheme {
  title?: string | any
  layout: 'mix' | 'side' | 'top'
  headerHeight: number
  logo?: string
  siderWidth: number // 侧边栏宽度
  siderCollapsedWidth: number // 侧边栏收起的宽度
  showSiderTrigger: boolean | 'bar' | 'arrow-circle' // 侧边栏默认的触发方式
  layoutStyle: 'inverted' | 'light' | 'dark'
  collapsed: boolean
}

export const layoutThemeConfig: LayoutTheme = {
  title: 'ellwood-admin-Naive-pro',
  layout: 'mix',
  headerHeight: 48,
  logo,
  siderWidth: 240,
  siderCollapsedWidth: 75,
  showSiderTrigger: 'arrow-circle',
  layoutStyle: 'inverted',
  collapsed: false
}
