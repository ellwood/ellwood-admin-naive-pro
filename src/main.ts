import { createApp } from 'vue'

import App from './App.vue'

import router, { setupRouter } from '~/router'
import { setupPlugins } from '~/pluins'

// 解决浏览器默认阻止了touchmove事件
import 'default-passive-events'

const meta = document.createElement('meta')
meta.name = 'naive-ui-style'
document.head.appendChild(meta)

async function bootstrap() {
  const app = createApp(App)

  // 插件配置
  setupPlugins(app)

  // 路由配置
  setupRouter(app)

  await router.isReady()

  app.mount('#app')
}

bootstrap().then()
