import type { LayoutTheme, LayoutType } from '~/config/layout-theme.ts'
import { layoutThemeConfig } from '~/config/layout-theme.ts'

/**
 * @ClassName: app
 * @Description: // 全局状态管理
 * @Author: Ellwood
 * @CreateDate: 2024/4/22 - 9:57
 * @Version: V1.0.0
 */
export const useAppStore = defineStore('app', () => {
  // 判断当前是不是在开发环境中，如果是在开发环境中那么我们直接使用默认的配置即可。
  const defaultLayout = import.meta.env.DEV
    ? layoutThemeConfig
    : useLayoutTheme()

  const layout = reactive(unref(defaultLayout))

  const updateLayout = (val: LayoutType['key']) => {
    layout.layout = val
  }

  const updateLayoutStyle = (val: LayoutTheme['layoutStyle']) => {
    layout.layoutStyle = val
  }

  const layoutList = computed<LayoutType[]>(() => {
    return [
      { id: 'mix', key: 'mix', title: '混合布局' },
      { id: 'side', key: 'side', title: '侧边布局' },
      { id: 'top', key: 'top', title: '顶部布局' }
    ]
  })

  const layoutStyleList = computed<LayoutType[]>(() => {
    const list: LayoutType[] = [{ id: 'light', key: 'side', title: '亮色风格' }]
    if (layout.layout !== 'mix') {
      list.push({
        id: 'inverted',
        key: 'side',
        inverted: true,
        title: '反转色风格'
      })
    } else {
      updateLayoutStyle('light')
      // if (layout.layoutStyle !== 'dark') updateLayoutStyle('light')
    }
    /* list.push({
               id: 'dark',
               key: 'side',
               title: '黑暗菜单风格',
               dark: true
             }) */
    return list
  })

  return {
    layout,
    layoutList,
    updateLayout,
    updateLayoutStyle,
    layoutStyleList
  }
})
