import { layoutThemeConfig } from '~/config/layout-theme.ts'

/**
 * @ClassName: layout-theme
 * @Description: // 全局配置布局持久化组合式api
 * @Author: Ellwood
 * @CreateDate: 2024/4/22 - 10:02
 * @Version: V1.0.0
 */
export const useLayoutTheme = createGlobalState(() =>
  useStorage('layout-theme', layoutThemeConfig)
)
